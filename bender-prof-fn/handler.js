'use strict'
const axios = require('axios');

module.exports = async (event, context) => {


  var resfn1 = axios.post('http://192.168.17.20:8080/function/bender-fn', 'hello');
  var resfn2 = axios.post('http://192.168.17.20:8080/function/bender-fn2', 'world');

  var x = await Promise.all([resfn1, resfn2]).then(([a, b]) => "result" + JSON.stringify(a.data) + JSON.stringify(b.data)).catch(console.log);

  return context
    .status(200)
    .succeed(x)
}
