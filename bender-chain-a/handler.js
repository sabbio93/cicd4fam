'use strict'
const axios = require('axios');

module.exports = async (event, context) => {
  const result = {
    'body': JSON.stringify(event.body),
    'content-type': event.headers["content-type"]
  }

  var bdy = "hello " + JSON.stringify(event.body);
  await axios.post('http://192.168.17.20:8080/async-function/bender-chain-b', bdy);


  return context
    .status(200)
    .succeed(result)
}
